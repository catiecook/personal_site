# personal_site

__explanation__
This is a new version of my website I made for the pre-course. I had been using floats for the div placements, and decided to try to translate that into flex boxes, which caused me to want to re do the whole site.

I have used divs to create all of the shapes, in effort to practice how to manipulate the divs and layout.

__flow__
The 'main.html' is the first page, which links to catie.html

__still working on__
I tried to implement a smooth transition using jquery, however I couldn't quite figure it out. I also wanted to set a div over the scales illustration that would make it look like the scales were loosing opacity just like the moons. However, I couldn't get it to sit over the div the way I wanted to. 
